<?php

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Etapa;
use app\models\Puerto;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>

        <h2>Ciclistas entre 20 y 30 años</h2>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => Ciclista::find()->where(['between', 'edad', 20, 30]),
            ]),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'nombre',
                'dorsal',
               /* 'edad', */ /*Te dejo el parametro edad comentado en caso de que quieras comprobar que la edad de cada ciclista
                * es entre 20 y 30*/
                
            ],
        ]) ?>

        <h2>Etapas circulares</h2>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => Etapa::find(),
            ]),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'numetapa',
                'kms',
            ],
        ]) ?>

        <h2>Puertos altura mayor a 1000m</h2>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => Puerto::find()->where(['>', 'altura', 1000]),
            ]),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'nompuerto',
                'dorsal',
               /* 'altura', */ /* Lo mismo aqui si quieres revisar, no supe como hacer lo de las etapas circulares*/
            ],
        ]) ?>

        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; Ceinmark <?= date('Y') ?></p>
        <p class="float-right"><?= 'Desarrollado con mucho cariño y MEJORADO por Guillermo :3' ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


/*lo que tengo que hacer para tener el vendor y los archivos que me faltan es usar el comando "Composer update? ¿por qué no lo pruebas?"